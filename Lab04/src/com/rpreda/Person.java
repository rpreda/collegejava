package com.rpreda;

import java.util.Scanner;

public class Person {
    public static void main(int n) {
        Scanner scn = new Scanner(System.in);
        Person[] data =  new Person[n];
        System.out.println("Input lat, lon, name for " + n + " persons");
        for (int i = 0; i < n; i++) {
            data[i] = new Person(scn.nextFloat(), scn.nextFloat(), scn.nextLine());
            System.out.println(data[i]);
        }
    }

    private float latitude, longitude;
    private String name;
    public Person() {
        longitude = 0;
        latitude = 0;
        name = null;
    }
    public Person(float latitude, float longitude, String data) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = data;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public String getName() {
        return name;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("Person: ")
                .append(name)
                .append(" is at: ")
                .append(latitude)
                .append(" ")
                .append(longitude).toString();
    }

    @Override @Deprecated
    protected void finalize() throws Throwable {
        System.out.println("Finalize!");
        super.finalize();
    }
}
