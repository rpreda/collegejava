package com.rpreda;

public class P5 {
    public static void CheckKey(String st) {
        if (st.matches("[a-zA-Z0-9]{5}-[a-zA-Z0-9]{5}-[a-zA-Z0-9]{5}-[a-zA-Z0-9]{5}")) {
            int digit_count = st.replaceAll("\\D", "").length();
            if (digit_count < st.length() - 3 - digit_count)
                System.out.println("Key okay!");
            else
                System.out.println("Invalid authentication key!”");
        }
        else
            System.out.println("Invalid authentication key!”");
    }
}
