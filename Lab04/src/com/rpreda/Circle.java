package com.rpreda;

public class Circle {
    private int color, radius, x, y;

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public int getColor() {
        return color;
    }

    public int getRadius() {
        return radius;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    private byte Quadrant() {
        if (x > 0 && y > 0)
            return 1;
        if (x < 0 && y > 0)
            return 2;
        if (x < 0 && y < 0)
            return 3;
        if (x > 0 && y < 0)
            return 4;
        return 0;
    }
    private boolean inQuadrant() {
        return (radius < x && radius < y);
    }
}
