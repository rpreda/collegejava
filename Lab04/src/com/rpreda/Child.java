package com.rpreda;

import java.util.Calendar;
import java.util.Date;

public class Child {
    private Date birthday;
    private String name;

    public Child(Date bithday, String name) {
        this.birthday = bithday;
        this.name = name;
    }

    public void sayAlphabet() {
        System.out.println("abcdefghijklmnopqrstuvwxyz");
    }

    public void sayReverseAlphabet() {
        System.out.println("zyxwvutsrqponmlkjihgfedcba");
    }

    public void sayBirthday() {
        System.out.println(birthday.toString());
    }

    public void sayAge() {
        System.out.println("I am " + (Calendar.getInstance().get(Calendar.YEAR) - birthday.getYear()) + " years old.");
    }

    public void sayGoodbye() {
        System.out.println("Goodbbye!");
    }

    public int[][] colorMatrix(int x, int y) {
        if (x < 0 || y < 0)
            return null;
        boolean val = false;
        int[][] data = new int[x][y];
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (val)
                    data[i][j] = 1;
                val = !val;
            }
            val = !val;
        }
        return data;
    }

    //TODO: Implement
    public void playTicTacToe() {

    }
}
