package com.rpreda;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Student {
    private String name, phone;
    private double mark;

    public Student(String n, String p, double m) {
        name = n;
        phone = p;
        mark = m;
    }

    public String getName() {
        return name;
    }

    public double getMark() {
        return mark;
    }

    public String getPhone() {
        return phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMark(double mark) {
        this.mark = mark;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    static class SortByGrade implements Comparator<Student>
    {
        // Used for sorting in ascending order of
        // roll number
        public int compare(Student a, Student b)
        {
            return (int)a.getMark() - (int)b.getMark();
        }
    }

    @Override
    public String toString() {
        return name + " " + phone + " " + mark + "\n";
    }

    public static void Run() {
        Scanner sc = new Scanner(System.in);
        int count = sc.nextInt();
        Student[] data = new Student[count];
        for (int i = 0; i < count; i++)
            data[i] = new Student(sc.nextLine(), sc.next(Pattern.compile("\\d{10}")), sc.nextDouble());
        Print(data);
        Arrays.sort(data, new SortByGrade());
        Print(data);
    }

    public static void Print(Student[] dat) {
        for (Student stud : dat)
            System.out.println(stud);
    }
}
