package com.rpreda;

public class Matrix_p1 {
    private int x, y;
    private int[][] data;
    public Matrix_p1() {
        x = 0; y = 0; data = null;
    }
    public Matrix_p1(int[][] mat) {
        x = mat.length;
        if (x > 0)
            y = mat[0].length;
        else
            System.out.println("Invalid matrix!");
        data = mat;
    }
    public Matrix_p1(int x, int y) {
        data = new int[x][y];
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int[][] getData() {
        return data;
    }

    public void setData(int[][] data) {
        this.data = data;
        x = data.length;
        if (x > 0)
            y = data[0].length;
    }

    @Override
    public String toString() {
        var sb = new StringBuilder();
        if (x > 0 && y > 0)
            for (int[] arr: data) {
                for (int i : arr)
                     sb.append(i);
                sb.append("\n");
            }
        return sb.toString();
    }

    public void printMat() {
        System.out.println(this.toString());
    }

    public int countBlocks(int value) {
        if (x <= 2 || y <= 2)
            return 0;
        float delta = 5.f / 100 * value;
        int block_count = 0;
        for (int i = 1; i < x - 1; i++) {
            for (int j = 1; j < y - 1; j++) {
                if (Math.abs(value - data[i][j]) < delta &&
                        Math.abs(value - data[i + 1][j]) < delta &&
                        Math.abs(value - data[i - 1][j]) < delta &&
                        Math.abs(value - data[i][j + 1]) < delta &&
                        Math.abs(value - data[i][j - 1]) < delta &&
                        Math.abs(value - data[i + 1][j + 1]) < delta &&
                        Math.abs(value - data[i - 1][j - 1]) < delta &&
                        Math.abs(value - data[i + 1][j - 1]) < delta &&
                        Math.abs(value - data[i - 1][j + 1]) < delta) {
                    block_count++;
                }
            }

        }
        return block_count;
    }
}
