package me.rpreda.p2;

import java.math.BigInteger;

public class Fraction {
    protected int a, b;

    public Fraction() {
    }

    public Fraction(int a, int b) throws Exception{
        this.a = a;
        if (b != 0)
            this.b = b;
        else throw new Exception();
    }

    public Fraction simplify() throws Exception {
        int div = cmmdc(a, b);
        return new Fraction(a/div, b/div);
    }

    protected int cmmdc(int n1, int n2) {
        for(int i = 2; i <= n1 && i <= n2; i++)
        {
            // Checks if i is factor of both integers
            if((n1 % i == 0) && (n2 % i == 0))
                return i;
        }
        return 1;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
}
