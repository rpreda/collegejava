package me.rpreda.p2;

public class ExtendedFraction extends Fraction {
    public ExtendedFraction(int a, int b) throws Exception {
        super(a, b);
    }

    @Override
    protected int cmmdc(int n1, int n2) {
        n1 = ( n1 > 0) ? n1 : -n1;
        n2 = ( n2 > 0) ? n2 : -n2;

        while(n1 != n2)
        {
            if(n1 > n2)
                n1 -= n2;
            else
                n2 -= n1;
        }
        return n1;
    }

    protected Fraction add(Fraction f) throws Exception {
        return new Fraction(a + f.getA(), b + f.getB());
    }

    protected Fraction subtract(Fraction f) throws Exception {
        return new Fraction(a - f.getA(), b - f.getB());
    }
}
