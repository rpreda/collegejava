package me.rpreda.p6;

public class Rook implements ChessPiece {
    int x, y;

    public Rook(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean move(int xNew, int yNew) {
        return ((Math.abs(xNew - x) > 1 && yNew - y == 0) || Math.abs(yNew - y) > 1 && x - xNew == 0);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
