package me.rpreda.p6;

public interface ChessPiece {
    boolean move(int xNew, int yNew);
}
