package me.rpreda.p6;

public class Pawn implements ChessPiece {
    private int x, y;
    @Override
    public boolean move(int xNew, int yNew) {
        if (Math.abs(x - xNew) > 1 || Math.abs(y - yNew) > 1)
            return false;
        x = xNew;
        y = yNew;
        return true;
    }

    public Pawn(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
