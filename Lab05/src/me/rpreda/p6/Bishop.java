package me.rpreda.p6;

public class Bishop implements ChessPiece {
    private int x, y;

    public Bishop(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean move(int xNew, int yNew) {
        if (Math.abs(x - xNew) != Math.abs(y - yNew))
            return false;
        return true;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
