package me.rpreda.p1;

public class Vehicle {
    private int maxPassengers, maxSpeed;
    private String color;

    public int getMaxPassengers() {
        return maxPassengers;
    }

    public void setMaxPassengers(int maxPassengers) {
        this.maxPassengers = maxPassengers;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Vehicle(int maxPassengers, int maxSpeed, String color) {
        this.maxPassengers = maxPassengers;
        this.maxSpeed = maxSpeed;
        this.color = color;
    }
}
