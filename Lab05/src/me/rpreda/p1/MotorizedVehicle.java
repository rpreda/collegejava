package me.rpreda.p1;

public class MotorizedVehicle extends Vehicle{
    protected int moveSpeed;// m/s
    protected GeoLoc location;

    public MotorizedVehicle(int maxPassengers, int maxSpeed, String color, int moveSpeed, GeoLoc location) {
        super(maxPassengers, maxSpeed, color);
        this.moveSpeed = moveSpeed;
        this.location = location;
    }

    public int getMoveSpeed() {
        return moveSpeed;
    }

    public void setMoveSpeed(int moveSpeed) {
        this.moveSpeed = moveSpeed;
    }

    public GeoLoc getLocation() {
        return location;
    }

    public void setLocation(GeoLoc location) {
        this.location = location;
    }
    public int move(GeoLoc b) {
        return (int)(distance(b.getLat(), b.getLng(), location.getLat(), location.getLng()) * 1000 / moveSpeed);
    }
    protected static double distance(double lat1, double lon1, double lat2, double lon2) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        }
        else {
            double theta = lon1 - lon2;
            double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344;
            return (dist);
        }
    }
}
