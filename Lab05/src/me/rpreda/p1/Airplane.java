package me.rpreda.p1;

public class Airplane extends MotorizedVehicle {
    private int altitude;

    public Airplane(int maxPassengers, int maxSpeed, String color, int moveSpeed, GeoLoc location, int altitude) {
        super(maxPassengers, maxSpeed, color, moveSpeed, location);
        this.altitude = altitude;
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    @Override
    public int move(GeoLoc b) {
        return (int)(distance(b.getLat(), b.getLng(), location.getLat(), location.getLng()) * 1000 / moveSpeed);
    }
}
