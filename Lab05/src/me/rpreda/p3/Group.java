package me.rpreda.p3;

public interface Group {
    void add(Child c);
    void remove(Child c);
    boolean isInGroup(Child c);
    void listMembers();
}
