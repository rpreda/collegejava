package me.rpreda.p3;

import java.util.ArrayList;
import java.util.List;

public class ChildrenGroup implements Group{
    private List<Child> lst = new ArrayList<Child>();

    @Override
    public void add(Child c) {
        lst.add(c);
    }

    @Override
    public void remove(Child c) {
        c.sayGoodbye();
        lst.remove(c);
    }

    @Override
    public boolean isInGroup(Child c) {
        return (lst.contains(c));
    }

    @Override
    public void listMembers() {
        for (Child ch : lst) {
            System.out.println(ch.getName());
        }
    }
}
