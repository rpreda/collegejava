package me.rpreda.p7;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class p7 {
    private void print(ArrayList<String> data) {
        for (String s : data)
            System.out.println(s);
    }
    public void run(ArrayList<String> data) {
        data.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return (o1.length() - o2.length());
            }
        });
        print(data);
        data.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return (o2.length() - o1.length());
            }
        });
        print(data);
        data.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1.startsWith("m") && !o2.startsWith("m"))
                    return -1;
                else if (!o1.startsWith("m") && o2.startsWith("m"))
                    return -1;
                else if (!o1.startsWith("m") && !o2.startsWith("m"))
                    return o2.charAt(0) - o1.charAt(0);
                return 0;
            }
        });
        print(data);
    }
}
