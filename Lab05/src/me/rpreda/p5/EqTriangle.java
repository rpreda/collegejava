package me.rpreda.p5;

public class EqTriangle implements GeometricForm {
    private double side;

    @Override
    public double getPerimeter() {
        return side * 3;
    }

    @Override
    public double getArea() {
        return Math.sqrt(3) / 4 * Math.pow(side, 2);
    }

    public EqTriangle(double side) {
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
}
