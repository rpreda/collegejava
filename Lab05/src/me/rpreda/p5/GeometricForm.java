package me.rpreda.p5;

public interface GeometricForm {
    double getArea();
    double getPerimeter();
}
