package dbInteraction;

public interface Auth {
    //createUser(), deleteUser() and login()
    public Person[] createUser(Person p);
    public Person[] deletUser(Person p);
    public Person[] login(Person p);
}
