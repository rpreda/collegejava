package dbInteraction;

public class Person {
    private String name, surname, email, uID, pass;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPass() {
        return pass;
    }

    public String getSurname() {
        return surname;
    }

    public String getuID() {
        return uID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setuID(String uID) {
        this.uID = uID;
    }
}
