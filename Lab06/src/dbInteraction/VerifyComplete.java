package dbInteraction;

public class VerifyComplete extends VerifyPerson {
    @Override
    public boolean VerifyPass() {
        return getPass().matches("(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[#$^+=!*()@%&]).{8,10}");
    }

    @Override
    public boolean VerifyUID() {
        return getuID().matches("[a-zA-Z.]");
    }
}
