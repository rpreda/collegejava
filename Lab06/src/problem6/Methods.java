package problem6;

public class Methods {
    public void superEx() throws SuperException {
        throw new SuperException();
    }
    public void smallerEx() throws SmallerException {
        throw new SmallerException();
    }
}
