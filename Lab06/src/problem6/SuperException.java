package problem6;

public class SuperException extends Exception {
    public SuperException() {
        System.out.println("SuperException");
    }

    public SuperException(String message) {
        super(message);
        System.out.println("SuperException");
    }

    public SuperException(String message, Throwable cause) {
        super(message, cause);
        System.out.println("SuperException");
    }

    public SuperException(Throwable cause) {
        super(cause);
        System.out.println("SuperException");
    }

    public SuperException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        System.out.println("SuperException");
    }
}
