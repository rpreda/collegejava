package problem6;

public class SmallerException extends SuperException {
    public SmallerException() {
        System.out.println("SmallerException");
    }

    public SmallerException(String message) {
        super(message);
        System.out.println("SmallerException");
    }

    public SmallerException(String message, Throwable cause) {
        super(message, cause);
        System.out.println("SmallerException");
    }

    public SmallerException(Throwable cause) {
        super(cause);
        System.out.println("SmallerException");
    }

    public SmallerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        System.out.println("SmallerException");
    }
}
