package problem4;

public class DoubleMatrx {
    double [][] matrixElems;

    public DoubleMatrx(double[][] matrixElems) {
        this.matrixElems = matrixElems;
    }

    public double[][] getMatrixElems() {
        return matrixElems;
    }

    public void setMatrixElems(double[][] matrixElems) {
        this.matrixElems = matrixElems;
    }

    public double atPosition(int x, int y) {
        try {
            return matrixElems[x][y];
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }
}
