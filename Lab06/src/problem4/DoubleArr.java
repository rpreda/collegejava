package problem4;

public class DoubleArr {
    double[] data;

    public DoubleArr(double[] data) {
        this.data = data;
    }

    public double[] getData() {
        return data;
    }

    public void setData(double[] data) {
        this.data = data;
    }

    public double atIndex(int indx) {
        try {
            return data[indx];
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }
}
