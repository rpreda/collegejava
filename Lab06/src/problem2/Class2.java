package problem2;

public class Class2 extends Class1 implements Int1, Int2 {
    private int one, two;

    public Class2(int a, int b, double c, double d) {
        super(c, d);
        one = a;
        two = b;
    }

    public int getOne() {
        return one;
    }

    public void setOne(int one) {
        this.one = one;
    }

    public int sum() {
        return one + two;
    }
    public double product() {
        return oned * twod;
    }
}
