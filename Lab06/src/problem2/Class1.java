package problem2;

public class Class1 {
    protected double oned, twod;

    public Class1(double oned, double twod) {
        this.oned = oned;
        this.twod = twod;
    }
    public double getOned() {
        return oned;
    }

    public double getTwod() {
        return twod;
    }

    public void setOned(double oned) {
        this.oned = oned;
    }

    public void setTwod(double twod) {
        this.twod = twod;
    }
}
