package imageProcessor;

public class Pixel {
    private short r, g, b;

    public short getB() {
        return b;
    }

    public short getG() {
        return g;
    }

    public short getR() {
        return r;
    }

    public void setB(short b) {
        if (b > 255)
            this.b = 255;
        else if(b < 0)
            this.b = 0;
        else
            this.b = b;
    }

    public void setG(short g) {
        if (g > 255)
            this.g = 255;
        else if(g < 0)
            this.g = 0;
        else
            this.g = g;
    }

    public void setR(short r) {
        if (r > 255)
            this.r = 255;
        else if(r < 0)
            this.r = 0;
        else
            this.r = r;
    }

    public void Black() {
        r = g = b = 0;
    }
}
