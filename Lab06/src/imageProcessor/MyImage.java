package imageProcessor;

public class MyImage {
    Pixel[][] data;

    public MyImage(Pixel[][] img) {
        data = img;
    }

    public Pixel[][] getData() {
        return data;
    }

    public void CancelBelow(int min) {
        for (Pixel[] p : data)
            for (Pixel px: p) {
                if (px.getB() < min || px.getG() < min || px.getR() < min)
                    px.Black();
            }
    }
    public void DeleteR() {
        for (Pixel[] p : data)
            for (Pixel px: p)
                px.setR((short)0);
    }

    public void DeleteG() {
        for (Pixel[] p : data)
            for (Pixel px: p)
                px.setG((short)0);
    }

    public void DeleteB() {
        for (Pixel[] p : data)
            for (Pixel px: p)
                px.setB((short)0);
    }

    public void Grayscale() {
        //0.21 R + 0.71 G + 0.07 B
        for (Pixel[] p : data)
            for (Pixel px: p) {
                short val = (short)(0.21 * px.getR() + 0.71 * px.getG() + 0.07 * px.getB());
                px.setR(val);
                px.setG(val);
                px.setB(val);
            }
    }

}
