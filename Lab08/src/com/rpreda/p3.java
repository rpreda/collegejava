package com.rpreda;


import java.util.Scanner;

public class p3 {
    public static void run() {
        Scanner scn = new Scanner(System.in);
        String date = "";
        System.out.println("Please enter date: ");
        while (!date.matches("^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}")) {
            System.out.println("Date format dd/mm/yyyy:");
            date = scn.nextLine();
        }
        String[] parts = date.split("\\/");
        int day = Integer.valueOf(parts[0]);
        int month = Integer.valueOf(parts[1]);
        int year = Integer.valueOf(parts[2]);
        System.out.println("Day:\n" + day);
        System.out.println("Month:");
        switch (month) {
            case 1:
                System.out.println("January");
                break;
            case 2:
                System.out.println("February");
                break;
            case 3:
                System.out.println("March");
                break;
            case 4:
                System.out.println("April");
                break;
            case 5:
                System.out.println("May");
                break;
            case 6:
                System.out.println("June");
                break;
            case 7:
                System.out.println("July");
                break;
            case 8:
                System.out.println("August");
                break;
            case 9:
                System.out.println("September");
                break;
            case 10:
                System.out.println("October");
                break;
            case 11:
                System.out.println("November");
                break;
            case 12:
                System.out.println("December");
                break;
        }
        System.out.println("Year:\n" + year);
        if (year % 4 == 0)
            System.out.println("Leap year!");
        while (!scn.nextLine().equals("x"));
    }
}
