package com.rpreda;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class p6 {
    public static void run(String filename) throws IOException {
        FileReader fr = new FileReader(filename);
        char[] buf = new char[256];
        fr.read(buf);
        fr.close();
        char[] pub = new char[256];
        InputStreamReader sr = new InputStreamReader(System.in);
        sr.read(pub);
        sr.close();
        char[] tid = new char[256];
        for (int i = 0; i < 256; i++)
            tid[i] = (char)(buf[i] ^ pub[i]);
        System.out.println(String.valueOf(tid));
    }
}
