package com.rpreda;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class p5 {
    public static void run(String filename) throws IOException {
        Scanner scn = new Scanner(new FileReader(filename));
        scn.useDelimiter("/");
        List<StudentData> data = new ArrayList<>();
        while (scn.hasNext())
            data.add(new StudentData(scn.next(), scn.next(), scn.next(), scn.next(), scn.next()));
        scn.close();
        List<StudentData> borninDec = new ArrayList<>();
        List<StudentData> namesAorN = new ArrayList<>();
        List<StudentData> pnumbers = new ArrayList<>();
        for (StudentData d : data) {
            if (d.getDateofbirth().contains("December"))
                borninDec.add(d);
            if (d.getName().equals("Andrei") || d.getName().equals("Nicolae"))
                namesAorN.add(d);
            if (!d.getPhone().contains("+40"))
                pnumbers.add(d);
        }
        writeFile("borninDec", borninDec);
        writeFile("namesAorN", namesAorN);
        writeFile("pnumbers", pnumbers);
    }
    private static void writeFile(String filename, List<StudentData> data) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
        for (StudentData d : data)
            writer.write(d.toString());
    }
}
