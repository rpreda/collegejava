package com.rpreda;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class p4 {
    public static void run() {
        try {
            FileInputStream input1 = new FileInputStream("file1.txt");
            FileInputStream input2 = new FileInputStream("file2.txt");
            SequenceInputStream stream = new SequenceInputStream(input1, input2);
            int j;
            String data = "";
            while ((j = stream.read()) != -1)
                data += (char)j;
            stream.close();
            String[] students = data.split("\n");
            List<String> studData = new ArrayList<>();
            for (String s : students)
                studData.add(s);
            studData.sort(new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o1.compareTo(o2);
                }
            });
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
