package com.rpreda;

import java.io.*;

public class p2 {
    @SuppressWarnings("Duplicates")
    public static void run(String filePath) {
        try {
            InputStreamReader iStreamReader = new FileReader(filePath);
            BufferedReader reader = new BufferedReader(iStreamReader);
            System.out.println("Give message:");
            String msg = reader.readLine();
            System.out.println("Give a day between 1-7");
            String day = reader.readLine();
            int dayNumber = Integer.valueOf(day);
            System.out.println("Give month");
            String month = reader.readLine();
            System.out.println("Give the year");
            int year = Integer.valueOf(reader.readLine());
            System.out.println(msg + " " + dayNumber + " " + month + " " + year);
            reader.readLine();
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
