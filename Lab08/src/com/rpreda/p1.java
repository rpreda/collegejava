package com.rpreda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class p1 {
    @SuppressWarnings("Duplicates")
    public static void run() {
        InputStreamReader iStreamReader = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(iStreamReader);
        try {
            System.out.println("Give message:");
            String msg = reader.readLine();
            System.out.println("Give a day between 1-7");
            String day = reader.readLine();
            int dayNumber = Integer.valueOf(day);
            System.out.println("Give month");
            String month = reader.readLine();
            System.out.println("Give the year");
            int year = Integer.valueOf(reader.readLine());
            System.out.println(msg + " " + dayNumber + " " + month + " " + year);
            reader.readLine();
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
