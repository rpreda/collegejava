package com.rpreda;

public class StudentData {
    //name, surname, phone
    //number, date of birth, link
    private String name, surname, phone, dateofbirth, link;

    public StudentData(String name, String surname, String phone, String dateofbirth, String link) {
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.dateofbirth = dateofbirth;
        this.link = link;
    }

    @Override
    public String toString() {
        return name + "/" + surname + "/" + phone + "/" +dateofbirth + "/" + link + "\n";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
