package me.rpreda;

import javax.swing.*;
import java.awt.*;

public class p2 extends JPanel {
    @Override
    public void paint(Graphics g) {
        g.setColor(Color.GREEN);
        g.drawOval(0, 0, 40, 40);
        g.fillOval(80, 70, 20, 20);
    }
    @SuppressWarnings("Duplicates")
    public static void run() {
        JFrame frame = new JFrame("P2");
        frame.getContentPane().add(new p2());
        frame.setSize(400, 400);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
    }
}
