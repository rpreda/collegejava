package me.rpreda;

import javax.swing.*;
import java.awt.*;
import java.util.Scanner;

public class p1 extends JPanel {
    private static String name, group;
    @Override
    public void paint(Graphics g) {
        g.setColor(Color.BLUE);
        g.drawString("Name: " + name + " Group: " + group, 400/2, 400/2);
    }

    public static void run() {
        Scanner scn = new Scanner(System.in);
        name = scn.nextLine();
        group = scn.nextLine();
        JFrame frame = new JFrame("P1");
        frame.getContentPane().add(new p1());
        frame.setSize(400, 400);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
    }
}
