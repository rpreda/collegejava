package me.rpreda;

import javax.swing.*;
import java.awt.*;

public class p4 extends JPanel {
    @Override
    public void paint(Graphics g) {
        g.setColor(new Color(0));
        g.fillRect(0, 0, 25, 400);
        g.setColor(new Color(1644825));
        g.fillRect(25, 0, 25, 400);
        g.setColor(new Color(3289650));
        g.fillRect(50, 0, 25, 400);
        g.setColor(new Color(4934475));
        g.fillRect(75, 0, 25, 400);
        g.setColor(new Color(6579300));
        g.fillRect(100, 0, 25, 400);
        g.setColor(new Color(8224125));
        g.fillRect(125, 0, 25, 400);
        g.setColor(new Color(9868950));
        g.fillRect(150, 0, 25, 400);
        g.setColor(new Color(11513775));
        g.fillRect(175, 0, 25, 400);
        g.setColor(new Color(13158600));
        g.fillRect(200, 0, 25, 400);
        g.setColor(new Color(14803425));
        g.fillRect(225, 0, 25, 400);
        g.setColor(Color.red);
        g.fillRect(250, 0, 25, 400);
        g.setColor(Color.green);
        g.fillRect(275, 0, 25, 400);
        g.setColor(Color.blue);
        g.fillRect(300, 0, 25, 400);
        g.setColor(Color.magenta);
        g.fillRect(325, 0, 25, 400);
        g.setColor(Color.cyan);
        g.fillRect(350, 0, 25, 400);
        g.setColor(Color.yellow);
        g.fillRect(375, 0, 25, 400);

    }

    @SuppressWarnings("Duplicates")
    public static void run() {
        JFrame frame = new JFrame("P4");
        frame.getContentPane().add(new p4());
        frame.setSize(400, 400);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
    }
}
