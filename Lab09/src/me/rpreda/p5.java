package me.rpreda;

import javax.swing.*;
import java.awt.*;

public class p5 extends JPanel {
    @Override
    public void paint(Graphics g) {
        int radius = 400;
        for (float angle = 0; angle < 2 * Math.PI; angle += 0.00001) {
            g.setColor(Color.getHSBColor((float)(angle / (Math.PI * 2)), 0.8f, 1f));
            g.drawLine(radius, radius, (int)(radius + Math.cos(angle) * radius), (int)(radius + Math.sin(angle) * radius));
        }
    }

    @SuppressWarnings("Duplicates")
    public static void run() {
        JFrame frame = new JFrame("P5");
        frame.getContentPane().add(new p5());
        frame.setSize(800, 900);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
    }
}
