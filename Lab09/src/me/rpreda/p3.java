package me.rpreda;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class p3 extends JPanel {
    @Override
    public void paint(Graphics g) {
        try {
            Image image = Toolkit.getDefaultToolkit().getImage("/Users/rpreda/pic.jpg");
            g.drawImage(image, 10, 10, this);
        }
        catch (Exception e) {
            e.getMessage();
        }
    }

    @SuppressWarnings("Duplicates")
    public static void run() {
        JFrame frame = new JFrame("P3");
        frame.getContentPane().add(new p3());
        frame.setSize(400, 400);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
    }
}
