package me.rpreda;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class p7 extends JPanel{
    @Override
    public void paint(Graphics g) {
        Random rand = new Random();
        for (int i = 0; i < 20; i++) {
            g.setColor(Color.getHSBColor(rand.nextFloat(), 0.8f, 1f));
            int diameter = rand.nextInt(600);
            g.drawOval(0, 0, diameter, diameter);
        }
    }
    @SuppressWarnings("Duplicates")
    public static void run() {
        JFrame frame = new JFrame("P7");
        frame.getContentPane().add(new p7());
        frame.setSize(600, 600);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
    }
}
