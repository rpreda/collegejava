package com.rpreda;

public class Point {
    public int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public Point(float x, float y) {
        this.x = Math.toIntExact(Math.round(x));
        this.y = Math.toIntExact(Math.round(y));
    }

}
