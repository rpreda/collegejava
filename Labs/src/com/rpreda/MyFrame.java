package com.rpreda;


import java.awt.Canvas;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.Dimension;
import java.awt.image.BufferedImage;



class MyFrame extends Frame implements WindowListener{

    MyCanvas canvas;


    MyFrame(String title){
        super(title);

        setLayout(new FlowLayout());
        canvas = new MyCanvas();
        add(canvas);

        this.addWindowListener(this);

        setSize(500, 500);
        setVisible(true);


    }

    @Override
    public void windowActivated(WindowEvent arg0) {
        // TODO Auto-generated method stub

    }
    @Override
    public void windowClosed(WindowEvent arg0) {
        // TODO Auto-generated method stub

    }
    @Override
    public void windowClosing(WindowEvent arg0) {
        System.exit(1);

    }
    @Override
    public void windowDeactivated(WindowEvent arg0) {
        // TODO Auto-generated method stub

    }
    @Override
    public void windowDeiconified(WindowEvent arg0) {
        // TODO Auto-generated method stub

    }
    @Override
    public void windowIconified(WindowEvent arg0) {
        // TODO Auto-generated method stub

    }
    @Override
    public void windowOpened(WindowEvent arg0) {
        // TODO Auto-generated method stub

    }



}

class MyCanvas extends Canvas{

    Graphics g;
    BufferedImage bi = null;
    float length_factor = 0.84f;
    private float angle_delta = (float)Math.PI * 0.07f;
    private float angle_mod1 = 1, angle_mod2 =1;
    float intial_length = 150;
    int max_l = 12;
    int size = 1000;

    MyCanvas(){
        this.setSize(new Dimension(size, size));
        this.setBackground(new Color(0, 0, 0));

        bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);

        drawIntoImage();
    }

    private void branchFractal(Graphics g, int level, float previous_angle, float length, Point start) {
        if (level < max_l) {
            Point next1, next2;
            float new_len = length_factor * length;
            float angle1 = previous_angle + angle_delta * angle_mod1;
            float angle2 = previous_angle - angle_delta * angle_mod2;
            next1 = new Point((float)Math.cos(angle1) * new_len, (float)Math.sin(angle1) * new_len);
            next1.x += start.x;
            next1.y += start.y;
            next2 = new Point((float)Math.cos(angle2) * new_len, (float)Math.sin(angle2) * new_len);
            next2.x += start.x;
            next2.y += start.y;
            g.drawLine(start.x, start.y, next1.x, next1.y);
            g.drawLine(start.x, start.y, next2.x, next2.y);
            branchFractal(g, level + 1, angle1, new_len, next1);
            branchFractal(g, level + 1, angle2, new_len, next2);
        }
    }

    public void drawIntoImage(){
        //desenarea in cadrul imaginii
        Graphics g = bi.getGraphics();
        g.setColor(Color.white);
        branchFractal(g, 0, (float)Math.PI / 2, intial_length, new Point(size/2, 0));
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(bi, 1, 1, this);
    }

}

