package me.rpreda;

public class P1 {
    private Long inputVal;
    public P1(Long num) {
        inputVal = num;
    }
    public void Run() {
        System.out.println("Binary: " + Long.toBinaryString(inputVal));
        System.out.println("Octal: " + Long.toOctalString(inputVal));
        System.out.println("Hexa: " + Long.toHexString(inputVal));
    }
}
