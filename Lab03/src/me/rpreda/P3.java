package me.rpreda;

public class P3 {
    private long val;

    public P3(long num) {
        val = num;
    }

    public void Run() {
        long mask1 = 0x00000000000000FF, mask2 = 0x000000000000FF00, mask3 = 0x0000000000FF0000, mask4 = 0x00000000FF000000;
        System.out.println("Initial val: "); new P1(val).Run();
        System.out.println("Octets: ");
        new P1((val & mask1)).Run();
        new P1((val & mask2) >> 8).Run();
        new P1((val & mask3) >> 16).Run();
        new P1((val & mask4) >> 24).Run();
    }
}
