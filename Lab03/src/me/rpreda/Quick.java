package me.rpreda;

import java.util.Arrays;
import java.util.Scanner;

public class Quick {

    private int[] note, credite;
    private int lNote, lCredite;
    int sumCred = 0, sumNote = 0;

    public Quick() {

        Scanner scn = new Scanner(System.in);
        System.out.println("Dati numarul de note si credite: ");
        lCredite = lNote = scn.nextInt();
        //lCredite = scn.nextInt();
        note = new int[lNote];
        credite = new int[lNote];
        System.out.println("Dati creditele intre 2 si 5: ");

        for (int i = 0; i < credite.length; i++) {
            boolean read = false;
            while (!read) {
                credite[i] = scn.nextInt();
                if (!(credite[i] < 2 || credite[i] > 5)) {
                    read = true;
                }
                else
                    System.out.println("Valoare incorecta!");
            }
        }
        System.out.println("Dati notele intre 1 si 10");
        for (int i = 0; i < note.length; i++) {
            boolean read = false;
            while (!read) {
                note[i] = scn.nextInt();
                if (!(note[i] < 1 || note[i] > 10)) {
                    read = true;
                    sumNote += credite[i] * note[i];
                }
                else
                    System.out.println("Valoare incorecta!");
            }
        }

        for (int a: credite) {
            sumCred += a;
        }

        System.out.println("Note: " + Arrays.toString(note));
        System.out.println("Credite: " + Arrays.toString(credite));
        System.out.println("Medie ponderata: ");
        System.out.printf("%.2f", (float)sumNote/sumCred);

    }
}
//Nota validata -> reintroducere 1 <= n <= 10 2->5 media de bursa - ponder average 30 credits or sum/ num grades include colocvii Admis 10 -> 2 Resp -> 0
//Afisare note, credite avg
