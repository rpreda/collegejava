package me.rpreda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class P2 {
    public static enum SortType {
        TWOFOR, BUBBLE, SYSTEM
    }

    public static Integer[] ReadArray() {
        List<Integer> ints = new ArrayList<Integer>();
        System.out.println("Give a negative number to stop the read: ");
        Scanner scan = new Scanner(System.in);
        int temp = 0;
        while (temp >= 0) {
            temp = scan.nextInt();
            ints.add(temp);
        }
        Integer[] out = new Integer[ints.size()];
        return (out = ints.toArray(out));
    }

    private Integer[] data;
    private SortType type;

    public P2(Integer[] data, P2.SortType type) {
        this.data = data;
        this.type = type;
    }


    private void Bubble() {
        boolean sorted = false;
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < data.length - 1; i++) {
                if (data[i] > data[i + 1]) {
                    int temp = data[i];
                    data[i] = data[i + 1];
                    data[i + 1] = temp;
                    sorted = false;
                }
            }
        }
    }

    private void TwoFor() {
        for (int i = 0; i < data.length; i++) {
            for (int j = i; j < data.length; j++) {
                if (data[i] > data[j]) {
                    int temp = data[i];
                    data[i] = data[j];
                    data[j] = temp;
                }
            }
        }
    }

    public Integer[] Run() {
        if (data.length <= 1) return data;
        switch (type) {
            case BUBBLE:
                Bubble();
                break;
            case TWOFOR:
                TwoFor();
                break;
            case SYSTEM:
                Arrays.sort(data);
                break;
        }
        System.out.println("Result: " + Arrays.toString(data));
        return data;
    }
}
