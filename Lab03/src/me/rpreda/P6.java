package me.rpreda;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class P6 {
    private List<String> cards = new ArrayList<String>();
    private Random rand = new Random();
    public P6() {
        String[] types = {"Spades", "Hearts", "Clubs", "Diamonds"};
        String[] specials = {"J", "K", "Q", "A"};
        for (String st: types) {
            for (Integer i = 2; i <= 10; i++) {
                cards.add(i.toString() + " of " + st);
            }
            for(String special: specials) {
                cards.add(special + " of " + st);
            }
        }
        System.out.println(cards.size() + " cards were generated!");
    }
    public void Run() {
        String extracted = "";
        int toExtract = 0;
        while (!extracted.contains("Spades")) {
            toExtract = Math.abs(rand.nextInt()) % cards.size();
            extracted = cards.get(toExtract);
            cards.remove(toExtract);
            System.out.println("Extracted: " + extracted);
            System.out.println("Cards left: " + cards.size());
            try {
                System.in.read();
            } catch (IOException e) {
                System.out.println("Error reading key");
            }
        }
    }
}
