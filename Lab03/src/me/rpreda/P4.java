package me.rpreda;

import java.util.Arrays;
import java.util.Scanner;

public class P4 {
    private int[][] data;
    private int min, max, x, y;

    public static int[][] Read() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Give x and y size: ");
        int n = scan.nextInt(), m = scan.nextInt();
        int[][] dat = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++)
                dat[i][j] = scan.nextInt();
            System.out.println();
        }
        return dat;
    }

    public P4(int[][] data, int min, int max, int x, int y) {
        this.data = data;
        this.min = min;
        this.max = max;
        this.x = x;
        this.y = y;
    }

    public void PrintStd() {
        for (int[] dat:
             data) {
            System.out.println(Arrays.toString(dat));
        }
    }

    public void PrintCol() {
        int max_len = -1;
        for (int[] dat:
             data) {
            if (max_len < dat.length)
                max_len = dat.length;
        }
        for (int i = 0; i < max_len; i++) {
            for (int j = 0; j < data.length; j++) {
                if (data[j].length >= i) {
                    System.out.print(" " + data[j][i]);
                }
                else
                    System.out.print("  ");
            }
            System.out.println();
        }
    }

    private void Neighbours() {
        try {
            System.out.println("" + " " + data[x][y + 1] + " " + data[x][y - 1] + " " +  data[x + 1][y] + " " +  data[x - 1][y]);
        }
        catch (Exception e) {
            System.out.println("Element at the border!");
        }
    }

    private void Zeros() {
        for (int[] dat:
             data) {
            for (int i = 0; i < dat.length; i++) {
                if (dat[i] < min || dat[i] > max)
                    dat[i] = 0;
            }
        }
    }

    public void Run() {
        PrintStd();
        PrintCol();
        Neighbours();
        Zeros();
        PrintStd();
    }
}