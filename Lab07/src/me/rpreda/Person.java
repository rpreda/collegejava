package me.rpreda;

public interface Person {
    int getAge();
    void setAge(int age);
    String getName();
    void setName(String name);
}
