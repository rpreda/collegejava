package me.rpreda;

public interface GenType<T> {
    public T getNext(T p);
}
