package me.rpreda;

import java.util.*;

public class UserFile {
    String name, extension;
    int size, type;

    public UserFile(String name, String extension, int type, int size) {
        this.name = name;
        this.extension = extension;
        this.type = type;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return name;
    }

    public static void run(Scanner scn) {
        Hashtable<Integer, String> types = new Hashtable<>();
        types.put(0, "text");
        types.put(1, "application");
        types.put(2, "image");
        types.put(3, "music");
        System.out.println("Types available");
        Set<Integer> keyset = types.keySet();
        for (Integer key : keyset)
            System.out.println("KEY: " + key + "TYPE: " + types.get(key));
        if (scn == null)
            scn = new Scanner(System.in);
        List<UserFile> data = new ArrayList<>();
        System.out.println("How many files will be entered?");
        int n = scn.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.println("Give: name, extension, type from table and size in bytes");
            data.add(new UserFile(scn.nextLine(), scn.nextLine(), scn.nextInt(), scn.nextInt()));
        }
        data.sort(new Comparator<UserFile>() {
            @Override
            public int compare(UserFile o1, UserFile o2) {
                return o1.getSize() - o2.getSize();
            }
        });
        for (UserFile uf : data)
            System.out.println(uf);
    }
}
