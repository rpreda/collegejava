package me.rpreda;

public class Calculator<T extends Number> {
    public double add(T a, T b) {
        return a.doubleValue() + b.doubleValue();
    }
    public double subtract(T a, T b) {
        return a.doubleValue() - b.doubleValue();
    }
    public double multiply(T a, T b) {
        return a.doubleValue() * b.doubleValue();
    }
    public double divide(T a, T b) {
        return a.doubleValue() / b.doubleValue();
    }
}


