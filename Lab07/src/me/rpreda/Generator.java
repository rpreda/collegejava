package me.rpreda;

public class Generator<T extends GenType> {
    public void next(T val) {
        System.out.println(val.getNext(val));
    }
}
