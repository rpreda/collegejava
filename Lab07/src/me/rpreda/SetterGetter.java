package me.rpreda;

public class SetterGetter<T extends Person> {
    public String getPersonName(T p) {
        return p.getName();
    }
    public void setPersonName(T p, String name) {
        p.setName(name);
    }
    public int getAge(T p) {
        return p.getAge();
    }
    public void setAge(T p, int age) {
        p.setAge(age);
    }
}
