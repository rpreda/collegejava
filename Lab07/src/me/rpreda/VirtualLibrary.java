package me.rpreda;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class VirtualLibrary<T extends Publication> {
    private List<Publication> data = new ArrayList<>();
    public void add(T t) {
        data.add(t);
    }
    public void remove(T t) {
        data.remove(t);
    }
    public boolean exists(T t) {
        return data.contains(t);
    }
    public void sort() {
        data.sort(new Comparator<Publication>() {
            @Override
            public int compare(Publication o1, Publication o2) {
                return o1.getTitle().compareTo(o2.getTitle());
            }
        });
    }
}
