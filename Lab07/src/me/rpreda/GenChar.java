package me.rpreda;

public class GenChar implements GenType<Character> {
    @Override
    public Character getNext(Character p) {
        return (char)(p + 1);
    }
}
