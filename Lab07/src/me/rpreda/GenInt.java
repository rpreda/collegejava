package me.rpreda;

public class GenInt implements GenType<Integer> {
    @Override
    public Integer getNext(Integer p) {
        return p + 1;
    }
}
