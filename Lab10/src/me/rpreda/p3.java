package me.rpreda;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("Duplicates")
public class p3 {
    public static void addComponents(Container pane) {
        pane.setLayout(null);

        JButton b1 = new JButton();
        JTextField name = new JTextField();
        JTextField faculty = new JTextField();
        JTextArea out = new JTextArea();
        JCheckBox finance = new JCheckBox("Financing");
        String[] years = {"1", "2", "3", "4"}, courses = {"AI", "Applied Electronics", "Communications", "Aliens"};
        JComboBox<String> year = new JComboBox(years);
        JComboBox<String> course = new JComboBox(courses);


        Insets insets = pane.getInsets();
        Dimension size = b1.getPreferredSize();
        b1.setBounds(insets.left, 400 + insets.top, size.width, size.height);
        b1.setText("Sign up");
        b1.addActionListener(e -> out.setText(name.getText() + " " + faculty.getText() + " Finacing " + finance.isSelected() + "\n" + year.getSelectedItem() + " " + course.getSelectedItem()));

        name.setBounds(insets.left, insets.top, 100, 40);
        name.setText("Name");
        faculty.setBounds(insets.left, 50 + insets.top, 100, 40);
        faculty.setText("Faculty");
        finance.setBounds(insets.left, 100 + insets.top, 100, 40);
        year.setBounds(insets.left, 150 + insets.top, 150, 40);
        course.setBounds(insets.left, 200 + insets.top, 150, 40);
        out.setBounds(insets.left, 300 + insets.top, 200, 100);
        out.setEditable(false);

        pane.add(name);
        pane.add(faculty);
        pane.add(out);
        pane.add(finance);
        pane.add(year);
        pane.add(course);
        pane.add(b1);

    }
    public static void createGUI() {
        JFrame frame = new JFrame("P3");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        addComponents(frame.getContentPane());

        Insets insets = frame.getInsets();
        frame.setSize(300 + insets.left + insets.right,
                500 + insets.top + insets.bottom);
        frame.setVisible(true);
    }
}
