package me.rpreda;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.swing.*;
import java.awt.*;

@SuppressWarnings("Duplicates")
public class p4 {
    public static void addComponents(Container pane) {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");

        JTextField out = new JTextField("");
        out.setEditable(false);

        var b1 = new JButton("1");
        b1.addActionListener(e->out.setText(out.getText() + "1"));
        var b2 = new JButton("2");
        b2.addActionListener(e->out.setText(out.getText() + "2"));
        var b3 = new JButton("3");
        b3.addActionListener(e->out.setText(out.getText() + "3"));
        var b4 = new JButton("4");
        b4.addActionListener(e->out.setText(out.getText() + "4"));
        var b5 = new JButton("5");
        b5.addActionListener(e->out.setText(out.getText() + "5"));
        var b6 = new JButton("6");
        b6.addActionListener(e->out.setText(out.getText() + "6"));
        var b7 = new JButton("7");
        b7.addActionListener(e->out.setText(out.getText() + "7"));
        var b8 = new JButton("8");
        b8.addActionListener(e->out.setText(out.getText() + "8"));
        var b9 = new JButton("9");
        b9.addActionListener(e->out.setText(out.getText() + "9"));
        var b10 = new JButton("0");
        b10.addActionListener(e->out.setText(out.getText() + "0"));
        var b11 = new JButton("-");
        b11.addActionListener(e->out.setText(out.getText() + "-"));
        var b12 = new JButton("+");
        b12.addActionListener(e->out.setText(out.getText() + "+"));
        var b13 = new JButton("*");
        b13.addActionListener(e->out.setText(out.getText() + "*"));
        var b14 = new JButton("/");
        b14.addActionListener(e->out.setText(out.getText() + "/"));
        var b15 = new JButton("Clear all");
        b15.addActionListener(e->out.setText(""));
        var b16 = new JButton("Clear one");
        b16.addActionListener(e-> {if (out.getText().length() >=  1) out.setText(out.getText().substring(0, out.getText().length() - 1));});
        var b17 = new JButton("Compute");
        b17.addActionListener(e-> { try {out.setText(engine.eval(out.getText()).toString());}catch (Exception ex) {}});

        pane.setLayout(new GridLayout(0, 3));
        pane.add(b1);
        pane.add(b2);
        pane.add(b3);
        pane.add(b4);
        pane.add(b5);
        pane.add(b6);
        pane.add(b7);
        pane.add(b8);
        pane.add(b9);
        pane.add(b10);
        pane.add(b11);
        pane.add(b12);
        pane.add(b13);
        pane.add(b14);
        pane.add(b15);
        pane.add(b16);
        pane.add(b17);
        pane.add(out);
    }
    public static void createGUI() {
        JFrame frame = new JFrame("P4");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        addComponents(frame.getContentPane());

        Insets insets = frame.getInsets();
        frame.setSize(300 + insets.left + insets.right,
                250 + insets.top + insets.bottom);
        frame.setVisible(true);
    }
}
