package me.rpreda;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;

@SuppressWarnings("Duplicates")
public class p1 {
    public static void addComponents(Container pane) {
        pane.setLayout(null);

        JButton b1 = new JButton();
        JTextField name = new JTextField();
        JTextField group = new JTextField();
        JTextField out = new JTextField();

        Insets insets = pane.getInsets();
        Dimension size = b1.getPreferredSize();
        b1.setBounds(insets.left, 200 + insets.top, size.width, size.height);
        b1.setText("Submit");
        b1.addActionListener(e -> out.setText(name.getText() + " " + group.getText()));

        name.setBounds(insets.left, insets.top, 100, 40);
        name.setText("Name");
        group.setBounds(insets.left, 50 + insets.top, 100, 40);
        group.setText("Group");
        out.setBounds(insets.left, 100 + insets.top, 100, 40);
        out.setEditable(false);
        out.setBackground(new Color(0x7A7B81));

        pane.add(name);
        pane.add(group);
        pane.add(out);
        pane.add(b1);

    }
    public static void createGUI() {
        JFrame frame = new JFrame("P1");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        addComponents(frame.getContentPane());

        Insets insets = frame.getInsets();
        frame.setSize(300 + insets.left + insets.right,
                250 + insets.top + insets.bottom);
        frame.setVisible(true);
    }
}
