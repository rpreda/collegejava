package me.rpreda;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;

public class p2 {
    private static void action(JLabel out, JTextField red, JTextField green, JTextField blue) {
        try {
            int r, g, b;
            r = Integer.valueOf(red.getText());
            g = Integer.valueOf(green.getText());
            b = Integer.valueOf(blue.getText());
            if (r > 255 || r < 0 || g > 255 || g < 0 || b > 255 || b < 0 )
                throw new Exception();
            out.setText("" + r + " " + g + " " + b);
            out.setForeground(new Color(r, g, b));
            System.out.println("OK");
        } catch (Exception e) {
            System.out.println("ERR");
            out.setText("ERROR!");
            out.setForeground(Color.red);
        }
    }
    @SuppressWarnings("Duplicates")
    private static void addComponents(Container pane) {
        pane.setLayout(null);

        JButton b1 = new JButton();
        JTextField red = new JTextField();
        JTextField green = new JTextField();
        JTextField blue = new JTextField();
        JLabel out = new JLabel();

        Insets insets = pane.getInsets();
        Dimension size = b1.getPreferredSize();
        b1.setBounds(insets.left, 200 + insets.top, size.width, size.height);
        b1.setText("Submit");
        b1.addActionListener(e -> p2.action(out, red, green, blue));
        out.setBounds(insets.left + 100, 200 + insets.top, 100, 20);

        red.setBounds(insets.left, insets.top, 100, 40);
        green.setBounds(insets.left, 50 + insets.top, 100, 40);
        blue.setBounds(insets.left, 100 + insets.top, 100, 40);


        pane.add(red);
        pane.add(green);
        pane.add(blue);
        pane.add(b1);
        pane.add(out);

    }
    @SuppressWarnings("Duplicates")
    public static void createGUI() {
        JFrame frame = new JFrame("P2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        addComponents(frame.getContentPane());

        Insets insets = frame.getInsets();
        frame.setSize(300 + insets.left + insets.right,
                250 + insets.top + insets.bottom);
        frame.setVisible(true);
    }
}
