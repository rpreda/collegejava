package me.rpreda;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@SuppressWarnings("Duplicates")
public class p6 {
    public static void addComponents(Container pane) {
        pane.setLayout(new GridLayout(0, 1));

        JButton b1 = new JButton("Select file");
        JTextArea out = new JTextArea("");

        final JFileChooser fc = new JFileChooser();

        b1.addActionListener((e) -> {
            int ret_val = fc.showDialog(pane, "Select file");
            if (ret_val == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                try {
                    FileInputStream fr = new FileInputStream(file);
                    byte[] data = new byte[(int) file.length()];
                    fr.read(data);
                    String str = new String(data);
                    out.setText(out.getText() + str);
                }
                catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        });

        out.setEditable(false);


        pane.add(b1);
        pane.add(out);

    }
    public static void createGUI() {
        JFrame frame = new JFrame("P6");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        addComponents(frame.getContentPane());

        Insets insets = frame.getInsets();
        frame.setSize(300 + insets.left + insets.right,
                250 + insets.top + insets.bottom);
        frame.setVisible(true);
    }
}
