package me.rpreda;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Random;

public class p5 {
    @SuppressWarnings("Duplicates")
    private static String p1_text = "P1", p2_text = "P2";
    private static boolean roll1 = false, roll2 = false;
    private static int one, two;
    private static JPanel panel;
    public static void createGUI() throws Exception {

        Random rand = new Random();

        Robot r = new Robot();
        JFrame frame = new JFrame("P5");
        panel = new JPanel() {
            @Override
            public void paint(Graphics g) {
                super.paint(g);
                g.setColor(Color.red);
                g.drawLine(150, 0, 150, 300);
                g.setColor(Color.BLACK);
                g.drawString(p1_text, 5, 10);
                g.drawString(p2_text, 155, 10);
            }
        };
        panel.addMouseListener(new MouseListener() {
            @Override
            @SuppressWarnings("Duplicates")
            public void mouseClicked(MouseEvent e) {
                if (e.getX() < 150) {
                    if (roll1 && roll2) {
                        roll1 = false;
                        roll2 = false;
                        p1_text = "";
                        p2_text = "";
                    }
                    if (!roll1) {
                        one = rand.nextInt(5) + 1;
                        while (roll2 && one == two)
                            one = rand.nextInt(5) + 1;
                        p1_text = "Roll: " + one;
                        roll1 = true;
                        if (roll2)
                        {
                            p1_text = (one > 2) ? "WIN " + one : "LOSS " + one;
                            p2_text = (one > 2) ? "LOSS " + two : "WIN " + two;
                        }
                    }
                }
                else {
                    if (roll1 && roll2) {
                        roll1 = false;
                        roll2 = false;
                        p1_text = "";
                        p2_text = "";
                    }
                    if (!roll2) {
                        two = rand.nextInt(5) + 1;
                        while (roll1 && one == two)
                            two = rand.nextInt(5) + 1;
                        p2_text = "Roll: " + two;
                        roll2 = true;
                        if (roll1)
                        {
                            p1_text = (one > 2) ? "WIN " + one : "LOSS " + one;
                            p2_text = (one > 2) ? "LOSS " + two : "WIN " + two;
                        }
                    }
                }
                panel.repaint();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        panel.setBounds(0, 0, 300, 300);
        frame.getContentPane().add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Insets insets = frame.getInsets();
        frame.setSize(300 + insets.left + insets.right,
                300 + insets.top + insets.bottom);
        frame.setVisible(true);
    }
}
