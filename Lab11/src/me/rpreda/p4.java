package me.rpreda;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class p4 {
    @SuppressWarnings("Duplicates")
    public static void createGUI() throws Exception {
        Robot r = new Robot();
        JFrame frame = new JFrame("P3");
        JPanel panel = new JPanel() {
            @Override
            public void paint(Graphics g) {
                super.paint(g);
                g.setColor(Color.red);
                g.fillRect(0, 20, 280, 20);
                g.setColor(Color.green);
                g.fillRect(100, 100, 20, 20);
            }
        };
        panel.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {

            }

            @Override
            public void mouseMoved(MouseEvent e) {
                if (e.getX() > 0 && e.getX() < 280 && e.getY() > 20 && e.getY() < 40)
                    r.mouseMove(frame.getX(), frame.getY());
                if (e.getX() > 100 && e.getX() < 120 && e.getY() > 100 && e.getY() < 120)
                    System.exit(0);

            }
        });
        panel.setBounds(0, 0, 300, 300);
        frame.getContentPane().add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Insets insets = frame.getInsets();
        frame.setSize(300 + insets.left + insets.right,
                300 + insets.top + insets.bottom);
        frame.setVisible(true);
    }
}
