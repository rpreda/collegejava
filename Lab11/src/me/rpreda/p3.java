package me.rpreda;
import javax.swing.*;
import java.awt.*;

@SuppressWarnings("Duplicates")
public class p3 {
    static Color c = Color.WHITE;
    static void addComponents(Container pane, JFrame frame) {
        pane.setLayout(null);
        JButton draw = new JButton("Draw");
        JSlider slider_r = new JSlider(0, 255);
        JSlider slider_g = new JSlider(0, 255);
        JSlider slider_b = new JSlider(0, 255);

        draw.setBounds(0, 120, 40, 20);
        draw.addActionListener(e -> frame.repaint());
        slider_r.setBounds(0, 150, 255, 20);
        slider_r.addChangeListener(e -> c = new Color(slider_r.getValue(), slider_g.getValue(), slider_b.getValue()));
        slider_g.setBounds(0, 180, 255, 20);
        slider_g.addChangeListener(e -> c = new Color(slider_r.getValue(), slider_g.getValue(), slider_b.getValue()));
        slider_b.setBounds(0, 210, 255, 20);
        slider_b.addChangeListener(e -> c = new Color(slider_r.getValue(), slider_g.getValue(), slider_b.getValue()));

        pane.add(draw);
        pane.add(slider_b);
        pane.add(slider_g);
        pane.add(slider_r);
    }
    public static void createGUI() {
        JFrame frame = new JFrame("P3") {
            @Override
            public void paint(Graphics g) {
                super.paint(g);
                g.setColor(p3.c);
                g.fillRect(0, 0, 100, 100);
            }
        };
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        addComponents(frame.getContentPane(), frame);

        Insets insets = frame.getInsets();
        frame.setSize(300 + insets.left + insets.right,
                300 + insets.top + insets.bottom);
        frame.setVisible(true);
    }
}